import express from "express"
import faker from "faker"

const app = express();


const generateProducts = (cantidad) => {
    const products = []

    for (let i = 0; i < cantidad; i++) {
        const product = {
            name: faker.commerce.productName(),
            price: faker.commerce.price(),
            photo: faker.image.imageUrl()
        }
        products.push(product);
    }
    return products
}
app.use("/productos/vista-test", (req, res) => {

    const { cant = 10 } = req.query;

    const products = generateProducts(cant);

    res.send({ products });
})


app.listen(3000, () => console.log("app runing on port 3000"))